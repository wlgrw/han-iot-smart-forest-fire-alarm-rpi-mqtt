# HAN IoT Smart Forest Fire Alarm RPi MQTT 

This application is developed for a **RaspberryPi 3B+**.
A SenseHAT needs to be available for showing some results by the led matrix.

## Building

The **Mosquitto** library must be installed: [Mosquitto](https://mosquitto.org/download/).
Necessary libs:

```bash
sudo apt install libssl-dev
sudo apt install libc-ares-dev
sudo apt install uuid-dev
sudo apt install libwebsockets-dev
```

Go to the RaspberyPI/sffasTTN directory.
Use the next command for compiling the RaspberryPi code:

```bash
make -j5
```

C++17 is used.

## Executing

The application **sffasTTN** does not need any command line parameters:

```bash
./sffasTTN
```

## Output example

Topic examples syntax:

    <app_id>/devices/<dev_id>/up

    <app_id>/devices/<dev_id>/events/activations

Topic example:

    han_ese_iot_sffa_test_development/devices/0004a30b0020105e/up

Some output in json format:

    [
        {
            "app_id": "han_ese_iot_sffa_test_development",
            "counter": 297,
            "dev_id": "0004a30b0020105e",
            "hardware_serial": "0004A30B0020105E",
            "metadata": {
                "airtime": 102656000,
                "coding_rate": "4/5",
                "data_rate": "SF7BW125",
                "frequency": 868.3,
                "gateways": [
                    {
                        "altitude": 9,
                        "channel": 1,
                        "gtw_id": "ttn_pe1mew_gateway_1",
                        "gtw_trusted": true,
                        "latitude": 52.21153,
                        "location_source": "registry",
                        "longitude": 5.983634,
                        "rf_chain": 1,
                        "rssi": -66,
                        "snr": 9,
                        "time": "2018-12-17T22:38:57Z",
                        "timestamp": 1700549459
                    }
                ],
                "modulation": "LORA",
                "time": "2018-12-17T22:38:57.852979502Z"
            },
            "payload_fields": {
                "analog_in_0": 3.29,
                "analog_in_50": -45.39,
                "analog_in_52": 89.1,
                "analog_in_53": 39.99,
                "analog_in_54": -100,
                "analog_in_55": 15,
                "presence_60": 0,
                "relative_humidity_11": 28,
                "temperature_1": 21.3,
                "temperature_10": 111
            },
            "payload_raw": "AAIBSQFnANUKZwRWC2g4MgLuRTQCIs41Ag+fNgLY8DcCBdw8ZgA=",
            "port": 1
        }
    ]

This application will show sensor data to the RapberryPi SenseHAT.
You need to configure **TTNconfig.h** to select a specific TTN sensor node device.
