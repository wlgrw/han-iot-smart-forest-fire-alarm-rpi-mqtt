#ifndef DISTANCELATLONG_H
#define DISTANCELATLONG_H

#include <cmath>

inline double deg2rad(double deg)
{
   return (deg * M_PI / 180);
}

inline double rad2deg(double rad)
{
   return (rad * 180 / M_PI);
}

/// Calculate distance between to points on the Earth in kilometers.
double calculateDistanceKM(double lat1d, double lon1d, double lat2d,
                           double lon2d);

#endif
