#include "DistanceLatLong.h"

namespace {
const double EARTH_RADIUS_KM{6371.0};
}

double calculateDistanceKM(double lat1d, double lon1d, double lat2d,
                           double lon2d)
{
   double lat1r{deg2rad(lat1d)};
   double lon1r{deg2rad(lon1d)};
   double lat2r{deg2rad(lat2d)};
   double lon2r{deg2rad(lon2d)};
   double u{sin((lat2r - lat1r) / 2)};
   double v{sin(lon2r - lon1r) / 2};

   double distanceKM{2.0 * EARTH_RADIUS_KM *
                     asin(sqrt(u * u + cos(lat1r) * cos(lat2r) * v * v))};

   return distanceKM;
}
