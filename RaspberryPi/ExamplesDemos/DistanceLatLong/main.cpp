#include "DistanceLatLong.h"

#include <iostream>

int main()
{
   std::cout << "Calculate distance between Amsterdan Centraal and Hilversum";

   std::cout << " = "
             << calculateDistanceKM(52.379189, 4.899431, 52.23, 5.11)
             << " km" << std::endl;

   return 0;
}