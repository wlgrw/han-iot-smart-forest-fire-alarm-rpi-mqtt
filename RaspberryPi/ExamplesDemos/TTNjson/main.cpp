#include <json.hpp>

#include <fstream>
#include <iostream>
#include <string>

using json = nlohmann::json;

int main(int argc, char *argv[])
{
   if (argc != 2) {
      std::cerr << "\n\tERROR: input file name missing\n\n";
      exit(EXIT_FAILURE); //===================================================>
   }

   std::ifstream jsfile(argv[1]);
   if (!jsfile) {
      std::cerr << "\n\tERROR: unknown input file\n\n";
      exit(EXIT_FAILURE); //===================================================>
   }

   json js;

   jsfile >> js;

   try {
      std::cout << js << "\n\n";
      std::cout << js.dump(4) << "\n\n";

      for (auto &item : js.items()) {
         std::cout << item.key() << " : " << item.value() << "\n\n";
      }

      std::cout << "#items = " << js.size() << "\n\n";
      for (auto &item : js) {
         std::cout << "app_id = " << item["app_id"] << "\n\n";
         std::cout << "dev_id = " << item["dev_id"] << "\n\n";
      }

      double temperature_1 = js[0]["payload_fields"]["temperature_1"];
      std::cout << "temperature_1 = " << temperature_1 << "\n";

      // json obects are containers, we can use iterators and the find function
      auto plfs = js[0].find("payload_fields");
      if (plfs != end(js[0])) {
         std::cout << "field 'payload_fields' found\n\n";
      }

      // Typo in json item name, this will throw an exception
      temperature_1 = js[0]["payload_field"]["temperature_1"];
      std::cout << "temperature_1 = " << temperature_1 << "\n";
   }
   catch (json::parse_error &e) {
      std::cerr << "\n\tERROR JSON: " << e.what() << "\n\n";
   }
   catch (json::out_of_range &e) {
      std::cerr << "\n\tERROR JSON: " << e.what() << "\n\n";
   }
   catch (json::type_error &e) {
      std::cerr << "\n\tERROR JSON: " << e.what() << "\n\n";
   }
   catch (std::exception &e) {
      std::cerr << "\n\tERROR: " << e.what() << "\n\n";
   }
   catch (...) {
      std::cerr << "\n\tERROR: UNKNOWN\n\n";
   }

   return 0;
}
