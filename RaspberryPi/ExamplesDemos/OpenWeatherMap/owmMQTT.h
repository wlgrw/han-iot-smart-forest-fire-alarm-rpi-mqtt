#ifndef OWMMQTT_H
#define OWMMQTT_H

#include "CommandProcessor.h"
#include "ParExe.h"

#include <string>
#include <vector>

using parameters_t = std::vector<std::string>;

/// #OwmMQTT is a MQTT client and uses an #ParExe object for executing an
/// external program in Python for retrieving weather data in JSON format.
/// Data source: OpenWeatherMap.org.
/// #ParExe will fork a task and redirects the output to this MQTT client.
/// @todo Initial version. #ParExe class needs improvements.
class OwmMQTT : public CommandProcessor
{
public:
   OwmMQTT(const std::string &appname, const std::string &clientname,
           const std::string &host, int port);
   virtual ~OwmMQTT() = default;
   OwmMQTT(const OwmMQTT &other) = delete;
   OwmMQTT &operator=(const OwmMQTT &other) = delete;

private:
   const std::string className_;
   const std::string mqttID_;
   ParExe weatherCommand_;
   std::string weatherData_;

   void getWeather(const parameters_t &commandParameters);
};

#endif
