import json
from urllib.request import urlopen

url = "http://samples.openweathermap.org/data/2.5/find?q=London&appid=b1b15e88fa797225412429c1c50c122a1"

try:
   response = urlopen(url)
   rss = response.read().decode('utf8')
   print(rss)
   parsed_json = json.loads(rss)

   print()
   print(parsed_json["list"][0]["name"])
   print()
   print(parsed_json["list"][0]["coord"])
   print()
   print(parsed_json["list"][0]["main"])
   print()
   print(parsed_json["list"][0]["main"]["pressure"])
   print()
   print(parsed_json["list"][0]["main"]["humidity"])
   print("PAREXE-READY")
   
except IOError:
   print("ERROR", url, "not connected")
   print("PAREXE-READY")

