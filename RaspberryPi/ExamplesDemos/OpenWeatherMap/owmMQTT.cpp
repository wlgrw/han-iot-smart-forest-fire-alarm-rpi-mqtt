#include "owmMQTT.h"
#include "CommandProcessor.h"
#include "MQTTconfig.h"
#include "ParExe.h"

#include <iostream>
#include <string>

#define CERR std::cerr << className_ << "::" << __func__ << "()\n   "

OwmMQTT::OwmMQTT(const std::string& appname,
                 const std::string& clientname,
                 const std::string& host,
                 int port) :
   CommandProcessor(appname, clientname, host, port),
   className_{__func__},
   mqttID_{HOSTNAME + appname + clientname},
   weatherCommand_{
      /// Resolved using your PATH environment variable.
      "python3",
      /// Explicit path necessary,
      /// not resolved using your PATH environment variable.
      "owm/owm.py",
      ""},
   weatherData_{"NOT AVAILABLE YET"}
{
   CERR << host << "'  port = " << port << "  id = " << mqttID_
        << "  topic root = " << MQTT_TOPIC_ROOT << std::endl;

   // MQTT commands
   // registerCommand("getWeather",
   //                bind(&OwmMQTT::getWeather, this, std::placeholders::_1));
   registerCommand("getWeather", [this](const parameters_t &commandParameters) {
      getWeather(commandParameters);
   });
}

void OwmMQTT::getWeather(const parameters_t &commandParameters)
{
   if (commandParameters.size() != 0) {
      publishError("getWeather", "number of parameters != 0");
   } else {
      weatherData_ = weatherCommand_.output();
      publishReturn("getWeather", weatherData_);
   }
}
