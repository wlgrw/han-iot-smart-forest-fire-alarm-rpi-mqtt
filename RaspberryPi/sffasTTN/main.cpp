// sffas

#include "AppInfo.h"
#include "SFFAS.h"
#include "TTNconfig.h"

#include <atomic>
#include <csignal>
#include <iostream>
#include <unistd.h>

using namespace std;

volatile sig_atomic_t receivedSIGINT{false};

void handleSIGINT(int /* s */)
{
   receivedSIGINT = true;
}

int main(int argc, char *argv[])
{
   int majorMosquitto{0};
   int minorMosquitto{0};
   int revisionMosquitto{0};

   string mqttBroker{TTN_MQTT_HOST};
   int mqttBrokerPort{TTN_MQTT_PORT};

   switch (argc) {
      case 1:
         break;
      default:
         cerr << "\nERROR no command line arguments allowed\n\n";
         exit(EXIT_FAILURE);
   }

   cout << "-- MQTT application: " << APPNAME_VERSION << "  ";
   mosqpp::lib_init();
   mosqpp::lib_version(&majorMosquitto, &minorMosquitto, &revisionMosquitto);
   cout << "uses Mosquitto lib version " << majorMosquitto << '.'
        << minorMosquitto << '.' << revisionMosquitto << endl;

   try {
      signal(SIGINT, handleSIGINT);

      SFFAS sffas("SFFAS", "sffas", mqttBroker, mqttBrokerPort);

      // Checking rc for reconnection, 'clients' is an initializer_list
      auto clients = {static_cast<mosqpp::mosquittopp *>(&sffas)};

      cout << "-- LoRaWAN for MQTT The Things Network is ready" << endl;

      while (!receivedSIGINT) {
         for (auto client : clients) {
            int rc = client->loop();
            if (rc) {
               cerr << "-- MQTT reconnect" << endl;
               client->reconnect();
            }
         }
      }
   }
   catch (exception &e) {
      cerr << "Exception " << e.what() << endl;
   }
   catch (...) {
      cerr << "UNKNOWN EXCEPTION\n";
   }

   cout << "-- MQTT application: " << APPNAME_VERSION << " stopped" << endl
        << endl;

   mosqpp::lib_cleanup();

   return 0;
}
