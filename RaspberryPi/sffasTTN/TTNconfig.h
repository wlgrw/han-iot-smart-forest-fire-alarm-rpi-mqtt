#ifndef TTNCONFIG_H
#define TTNCONFIG_H

#include <string>

// const std::string TTN_APP_ID{"han_demo_test_application"};
// const std::string TTN_APP_KEY{
//    "ttn-account-v2.wvrKQt7M6tCqjsqZt27Nx9XdaJ5IT-It9c5cqs42ZUw"};

const std::string TTN_APP_ID{"han_ese_iot_sffa_test_development"};
const std::string TTN_ACC_KEY{
   "ttn-account-v2.UgUbr8T8UanQZnFwK6IFPgm9g8vavn8LRz6Bb-Z4F4w"};
/// ttn-handler-eu
const std::string TTN_REGION{"eu"};

const std::string TTN_MQTT_HOST{TTN_REGION + ".thethings.network"};
const int TTN_MQTT_PORT{1883};
const int TTN_KEEP_ALIVE{120};

const std::string TTN_TOPIC_ACTIVATIONS{"+/devices/+/events/activations"};
const std::string TTN_TOPIC_UP{"+/devices/+/up"};

const std::string TTN_DEV_ID{"0004a30b001fbc91"};
const std::string TTN_PAYLOAD_FIELD{"analog_in_50"};
const double TTN_DATA_MAX{100};
const double TTN_DATA_MIN{-100};
const std::string TTN_DATA_NAME{"sine (-100 ... +100)"};

#endif