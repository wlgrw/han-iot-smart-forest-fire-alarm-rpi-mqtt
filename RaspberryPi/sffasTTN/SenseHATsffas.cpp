#include "SenseHATsffas.h"
#include "LedMatrix.h"

const Pixel SenseHATsffas::background_{Pixel{0, 50, 0}};

SenseHATsffas::SenseHATsffas()
   : SenseHAT{}
   , column_{0}
{
   leds.clear(background_);
   // bind() creates a function object:
   // stick.directionPRESSED = std::bind(&SenseHATsffas::resetTimeSeries, this);
   // Prefer a lambda function calling a class member function:
   stick.directionPRESSED = [this]() { resetTimeSeries(); };
   // And: 
   stick.directionRIGHT = [this]() {
      leds.clear(background_);
      column_ = 3;
   };
}

void SenseHATsffas::drawTimeSeries(double value, const double min,
                                   const double max, const Pixel &pixel)
{
   // MIN_Y is at the top and MAX_Y is at the bottom.
   // Mapping: Scaling = a and shifting = b
   // System of 2 equations for mapping value to led matrix y coordinate:
   // (1) MIN_Y = a.max + b
   // (2) MAX_Y = a.min + b
   double a{(LedMatrix::MIN_Y - LedMatrix::MAX_Y) / (max - min)};
   double b{LedMatrix::MAX_Y - (a * min)};
   // Scaling = a and shifting = b
   int y = a * value + b;

   drawColumn(column_, background_);
   drawColumn((column_ + 1) % (LedMatrix::MAX_Y + 1), Pixel::BLANK);
   leds.setPixel(column_, y, pixel);
   ++column_;
   column_ %= (LedMatrix::MAX_X + 1);
}

void SenseHATsffas::drawColumn(int column, const Pixel &pixel)
{
   for (auto y = LedMatrix::MIN_Y; y <= LedMatrix::MAX_Y; ++y) {
      leds.setPixel(column, y, pixel);
   }
}

void SenseHATsffas::resetTimeSeries()
{
   leds.clear(background_);
   column_ = 0;
}
