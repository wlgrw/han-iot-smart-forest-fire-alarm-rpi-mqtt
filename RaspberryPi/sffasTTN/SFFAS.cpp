#include "SFFAS.h"
#include "MQTTconfig.h"
#include "MQTTmessage.h"
#include "TTNconfig.h"
#include "Topic.h"
#include "json.hpp"

#include <iostream>
#include <mosquitto.h>
#include <mosquittopp.h>
#include <string>

#define CERR std::cerr << className_ << "::" << __func__ << "()\n   "

SFFAS::SFFAS(const std::string &appname, const std::string &clientname,
             const std::string &host, int port)
   : mosqpp::mosquittopp{(HOSTNAME + appname + clientname).c_str()}
   , leds{}
   , className_{__func__}
   , mqttID_{HOSTNAME + appname + clientname}
   , queue_{bind(&SFFAS::handle, this, std::placeholders::_1)}
{
   CERR << "connect() host = '" << host << "'  port = " << port
        << "  MQTTid = " << mqttID_ << std::endl;
   username_pw_set(TTN_APP_ID.c_str(), TTN_ACC_KEY.c_str());
   connect(host.c_str(), port, TTN_KEEP_ALIVE);
}

SFFAS::~SFFAS()
{
   disconnect();
   CERR << " disconnect()" << std::endl;
}

void SFFAS::on_connect(int rc)
{
   CERR << "connected with rc = " << rc << " '" << mosquitto_strerror(rc) << "'"
        << std::endl;
   if (rc == 0) {
      /// Only attempt to subscribe on a successful connect.
      int rc1 = subscribe(nullptr, TTN_TOPIC_UP.c_str(), MQTT_QoS_0);
      CERR << " " << TTN_TOPIC_UP << std::endl;
      if (rc1 != MOSQ_ERR_SUCCESS) {
         on_log(1, mosquitto_strerror(rc1));
      }
      int rc2 = subscribe(nullptr, TTN_TOPIC_ACTIVATIONS.c_str(), MQTT_QoS_0);
      CERR << " " << TTN_TOPIC_ACTIVATIONS << std::endl;
      if (rc2 != MOSQ_ERR_SUCCESS) {
         on_log(1, mosquitto_strerror(rc2));
      }
   }
}

void SFFAS::on_disconnect(int rc)
{
   CERR << " disconnected with rc = " << rc << " '" << mosquitto_strerror(rc)
        << "'" << std::endl;
}

void SFFAS::on_message(const mosquitto_message *message)
{
   MQTTmessage msg(message);

   json jsonMsg{json::parse(msg.getPayload())};
   auto tm = make_pair(msg.getTopic(), jsonMsg);
   queue_.provide(tm);
}

void SFFAS::on_subscribe(int mid, int qos_count, const int *granted_qos)
{
   CERR << "Subscription succeeded, mid = " << mid
        << " qos_count = " << qos_count << " granted_qos = " << *granted_qos
        << std::endl;
}

void SFFAS::on_log(int level, const char *str)
{
   CERR << " level = " << level << ": " << str << std::endl;
}

void SFFAS::on_error()
{
   CERR << " ERROR " << std::endl;
}

void SFFAS::handle(const topic_message_t &topic_message)
{
   Topic topic{topic_message.first};

   CERR << "Topic = " << topic << "\n";

   if (topic.isPresent(TTN_DEV_ID)) {
      CERR << topic_message.second.dump(4) << "\n";

      auto plfs = topic_message.second[0].find("payload_fields");

      if (plfs != end(topic_message.second[0])) {
         auto plfdata = plfs->find(TTN_PAYLOAD_FIELD);
         if (plfdata != end(*plfs)) {
            double value =
               topic_message.second[0]["payload_fields"][TTN_PAYLOAD_FIELD];
            CERR << TTN_DATA_NAME << " = " << value << "\n";

            Pixel pdata{value < 0 ? Pixel::BLUE : Pixel::RED};
            leds.drawTimeSeries(value, TTN_DATA_MIN, TTN_DATA_MAX, pdata);
         } else {
            CERR << TTN_PAYLOAD_FIELD << " not available\n";
         }
      }
   }
}