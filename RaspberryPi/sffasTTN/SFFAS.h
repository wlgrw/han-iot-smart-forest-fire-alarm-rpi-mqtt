#ifndef SFFAS_H
#define SFFAS_H

#include "ParQueue.h"
#include "SenseHATsffas.h"

#include <json.hpp>
#include <mosquittopp.h>
#include <string>

using json = nlohmann::json;

/// The class #SFFAS is a MQTT client class, public derived
/// from mosqpp::mosquittopp.
/// SFFAS contains a SenseHATsffas object (composition).
class SFFAS : public mosqpp::mosquittopp
{
public:
   using topic_message_t = std::pair<std::string, json>;

   SFFAS(const std::string &appname, const std::string &clientname,
         const std::string &host, int port);
   SFFAS(const SFFAS &other) = delete;
   SFFAS &operator=(const SFFAS &other) = delete;
   virtual ~SFFAS();

   SenseHATsffas leds;

protected:
   const std::string className_;
   const std::string mqttID_;

   void on_connect(int rc) override;
   void on_disconnect(int rc) override;
   void on_message(const struct mosquitto_message *message) override;
   void on_subscribe(int mid, int qos_count, const int *granted_qos) override;
   void on_log(int level, const char *str) override;
   void on_error() override;

private:
   ParQueue<topic_message_t> queue_;

   void handle(const topic_message_t& topic_message);
};

#endif