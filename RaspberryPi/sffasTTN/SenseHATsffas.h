#ifndef SHSFFAS_H
#define SHSFFAS_H

#include "Pixel.h"
#include "SenseHAT.h"

/// SenseHATsffas IS-A SenseHAT (public derivation) specialised for showing
/// sffas data to the led matrix.
/// \todo Add functions for showing data related pixel patterns.
/// \bug crashes if json item is not present
class SenseHATsffas : public SenseHAT
{
public:
   SenseHATsffas();
   virtual ~SenseHATsffas() = default;

   void drawTimeSeries(double value, const double min, const double max,
                       const Pixel &pixel);
   void resetTimeSeries();

private:
   const static Pixel background_;
   int column_;

   void drawColumn(int column, const Pixel &pixel);
};

#endif